package com.example.demoMvcBoot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoMvcBoot.beans.Car;

@RestController
public class TestController {

    @GetMapping("/cars")
    Car getCars(){
        Car car = new Car("asd", "red");
        return car;
    }


}
