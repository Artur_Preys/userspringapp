package com.example.demoMvcBoot.beans;

import static com.example.demoMvcBoot.utils.BigdecimalUtils.amount;
import static com.example.demoMvcBoot.utils.BigdecimalUtils.isL;

import java.math.BigDecimal;
import java.time.temporal.ValueRange;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Employee {

    public static final BigDecimal MINIMAL_SALARY = amount("350.00");

    private String name;

    private int age;

    private BigDecimal salary;

    private String email;

    private String phone;

    public void setSalary(BigDecimal salary) {
        if (isL(salary, amount(0))) {
            throw new IllegalArgumentException("Salary could not be negative");
        }
        if (isL(salary, MINIMAL_SALARY)) {
            this.salary = MINIMAL_SALARY;
        } else {
            this.salary = salary;
        }
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setAge(int age) {
        if (ValueRange.of(18, 65).isValidIntValue(age)) {
            this.age = age;
        } else {
            throw new IllegalArgumentException("Age not in range");
        }
    }

    public int getAge() {
        return age;
    }

    public void setEmail(String email) {

        if (isValidEmailId(email)) {
            this.email = email;
        } else {
            throw new IllegalArgumentException("Email is not valid");
        }
    }

    public String getEmail() {
        return email;
    }

    public static boolean isValidEmailId(String email) {
        String emailPattern = "[a-z]{3,20}@+[a-z]{3,20}.[a-z]{2,3}";
        Pattern p = Pattern.compile(emailPattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        if(isValidPhone(phone)){
            this.phone = phone;
        }
        else{
            throw new IllegalArgumentException("Phone is not valid");
        }
    }

    public static boolean isValidPhone(String phone) {
        String phonePattern = "[+](372)[0-9]{8}";
        Pattern p = Pattern.compile(phonePattern);
        Matcher m = p.matcher(phone);
        return m.matches();
    }
}
