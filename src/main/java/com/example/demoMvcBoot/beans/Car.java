package com.example.demoMvcBoot.beans;

public class Car {

    public Car(String name, String color) {
        this.name = name;
        this.color = color;
    }

    private String name;
    private String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
