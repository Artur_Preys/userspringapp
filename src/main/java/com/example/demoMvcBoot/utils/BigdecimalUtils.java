package com.example.demoMvcBoot.utils;

import java.math.BigDecimal;

public class BigdecimalUtils {

    public static final int SUBUNIT_SCALE = 0;
    public static final int AMOUNT_SCALE = 2;
    public static final BigDecimal ZERO = amount(0);

    private BigdecimalUtils() {
    }

    public static BigDecimal amount(BigDecimal amount) {
        return amount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_UP);
    }

    public static BigDecimal amount(int value) {
        return amount(new BigDecimal(value));
    }


    public static boolean isLe(BigDecimal left, BigDecimal right) {
        int compareTo = left.compareTo(right);
        return compareTo <= 0;
    }

    public static boolean isGe(BigDecimal left, BigDecimal right) {
        int compareTo = left.compareTo(right);
        return compareTo >= 0;
    }

    public static boolean isG(BigDecimal left, BigDecimal right) {
        int compareTo = left.compareTo(right);
        return compareTo > 0;
    }

    public static boolean isL(BigDecimal left, BigDecimal right) {
        int compareTo = left.compareTo(right);
        return compareTo < 0;
    }

    public static BigDecimal amount(String value) {
        if (value == null) {
            return null;
        }
        return amount(new BigDecimal(value));
    }

}
