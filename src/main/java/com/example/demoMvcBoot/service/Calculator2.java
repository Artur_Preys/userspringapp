package com.example.demoMvcBoot.service;

public class Calculator2 {

    double add(double a , double b){
        return a+b;
    }

    double multiply(double a , double b){
        return a*b;
    }

    public double divide(int a, int b) {
        try {
            return a / b;
        } catch (Exception ex){
         throw new IllegalArgumentException("Divide by 0");

        }


    }

    public double reverseSign(double a) {
        return -a;

    }

    public double sub(double a, double b) {
        return a-b;
    }
}
