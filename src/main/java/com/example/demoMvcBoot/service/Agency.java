package com.example.demoMvcBoot.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demoMvcBoot.beans.Car;

@Component
public class Agency {

    public List<Car> findCar(int i, String sedan) {
        Car car1 = new Car("audi", "red");
        ArrayList<Car> cars= new ArrayList<>();
        cars.add(car1);
        return cars;
    }
}
