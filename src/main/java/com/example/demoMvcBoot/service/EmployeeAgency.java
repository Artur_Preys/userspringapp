package com.example.demoMvcBoot.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demoMvcBoot.beans.Employee;

@Component
public class EmployeeAgency {

    private final ExternalAgencyService externalAgencyService;

    public EmployeeAgency(ExternalAgencyService externalAgencyService) {
        this.externalAgencyService = externalAgencyService;
    }

    public List<Employee> getEmployeesinRange(int min , int max){
       return externalAgencyService.getEmployees(min, max);
    }

    public List<Employee> getEmployeesByProfession(String title){
        return externalAgencyService.getEmployeesByTitle(title);
    }
}
