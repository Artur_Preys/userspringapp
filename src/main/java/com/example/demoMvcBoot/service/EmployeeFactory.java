package com.example.demoMvcBoot.service;

import static com.example.demoMvcBoot.utils.BigdecimalUtils.amount;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demoMvcBoot.beans.Employee;

@Component
public class EmployeeFactory {

    private final EmployeeAgency employeeAgency;

    public EmployeeFactory(EmployeeAgency employeeAgency) {
        this.employeeAgency = employeeAgency;
    }

    public List<Employee> updateEmployeesSalaries(){
        List<Employee> employees = employeeAgency.getEmployeesByProfession("Programmer");
        employees.forEach(it-> it.setSalary(amount("1500")));
        return employees;
    }
}
