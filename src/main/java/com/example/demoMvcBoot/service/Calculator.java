package com.example.demoMvcBoot.service;

public class Calculator {

    public int multiply(int a, int b) {
        return a * b;
    }

    public int divide(int a, int b) {
        try {
            return a / b;
        } catch (ArithmeticException ex) {
            throw new IllegalArgumentException("Divide by 0");
        }
    }

    public double reverseSign(double a) {
        return -a;
    }

    public double add(double a, double b) {
        return a + b ;
    }

    public double sub(double a, double b) {
        return a-b;
    }
}
