package com.example.demoMvcBoot.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public User(String name, String s) {
        this.name = name;
        this.email = s;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
    public User(){}

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private  String name;
    private  String email;

    // standard constructors / setters / getters / toString
}
