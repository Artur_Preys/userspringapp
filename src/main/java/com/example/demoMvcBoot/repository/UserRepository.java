package com.example.demoMvcBoot.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demoMvcBoot.domain.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {}
