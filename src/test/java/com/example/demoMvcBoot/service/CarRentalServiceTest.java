package com.example.demoMvcBoot.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.startsWith;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.demoMvcBoot.beans.Car;

@ExtendWith(MockitoExtension.class)
public class CarRentalServiceTest {

    @Mock
    Agency agencyMock;

    @Test
    void verifyExample1() {
        agencyMock.findCar(5, "sedan");
        verify(agencyMock).findCar(5, "sedan");
    }

    @Test
    void verifyExample2() {
        agencyMock.findCar(5, "sedan");
        agencyMock.findCar(123, "cab");
        agencyMock.findCar(234, "truck");
        agencyMock.findCar(23423, "sedan");
        verify(agencyMock, atMost(10)).findCar(any(Integer.class), anyString());
    }

    @Test
    void verifyExample3() {
        agencyMock.findCar(5, "sedan");
        verify(agencyMock, atLeastOnce()).findCar(eq(5), startsWith("sed"));
    }

    @Test
    void whenExample1() {
        Agency realAgency = new Agency();
        List<Car> cars = agencyMock.findCar(5, "sedan");
        assertNotNull(cars);
        assertTrue(cars.isEmpty());

        List<Car> realCars = realAgency.findCar(5, "sedan");
        assertNotNull(realCars);
        assertFalse(realCars.isEmpty());
    }

    @Test
    void whenExample2() {
        Car someCar = new Car("Ford", "blue");
        when(agencyMock.findCar(5, "sedan"))
            .thenReturn(Collections.singletonList(someCar));
        List<Car> result = agencyMock.findCar(5, "sedan");
        assertEquals(1, result.size());
        assertEquals(someCar, result.get(0));
    }




}
