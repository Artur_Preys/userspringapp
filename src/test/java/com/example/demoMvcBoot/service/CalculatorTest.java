package com.example.demoMvcBoot.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Month;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import com.example.demoMvcBoot.beans.Car;

class CalculatorTest {

    Calculator calculator = new Calculator();

    @BeforeEach
    public void setUp() {
        System.out.println("Print before each test");
    }

    @AfterEach
    public void after() {
        System.out.println("Print after each test");
    }

    @Test
    public void testAssertions() {
        assertEquals(64, 2 * 32);
        int x = 5;
        int y = 10;
        assertTrue(x < y);
        assertFalse(x > y);
        String[] array1 = new String[3];
        String[] array2 = new String[3];
        array1[0] = "sda";
        array2[0] = "sda";
        assertArrayEquals(array1, array2);
        Car car1 = null;
        Car car2 = new Car("opel", "red");
        Car car3 = car2;
        assertNull(car1);
        assertNotNull(car2);
        assertSame(car3, car2);
    }

    @Test
    public void shouldReturnMultiplicationOperation() {
        assertAll(
            () -> assertEquals(4, calculator.multiply(2, 2)),
            () -> assertEquals(81, calculator.multiply(9, 9)),
            () -> assertEquals(30, calculator.multiply(5, 6))
        );
    }

    @Test
    public void shouldAcceptDivideByZero() {
        IllegalArgumentException exception =
            Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    calculator.divide(10, 0);
                });
        assertEquals("Divide by 0", exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(doubles = { 10.0, -23.0, 12.0, -2.0, 4.0, 5.77 })
    public void shouldReturnReverseSign(double a) {
        assertEquals(-1 * a, calculator.reverseSign(a));
    }

    @ParameterizedTest
    @MethodSource(value = "getParameters")
    public void shouldReturnReverseSing2(double a) {
        assertEquals(-1 * a, calculator.reverseSign(a));
    }

    static Stream getParameters() {
        return Stream.of(1.0, -233.0, 26.4, -98.4, 100.0);
    }

    @ParameterizedTest
    @ArgumentsSource(Parameters.class)
    public void shouldReturnReverseSing3(double a) {
        assertEquals(-1 * a, calculator.reverseSign(a));
    }

    static class Parameters implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
            return Stream.of(23.0, -56.0, 64.92, -0.32)
                .map(Arguments::of);
        }
    }

    @ParameterizedTest
    @CsvSource({ "10.0, 10.0, 20.0", "13.4, 2.9, 16.3", "123.2, 5.3, 128.5" })
    public void shouldReturnAdditionOperation(double a, double b, double sum) {
        assertEquals(sum, calculator.add(a, b));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/dataSource.csv")
    public void shouldReturnSubtractionOperation(double a, double b, double sub) {
        assertEquals(sub, calculator.sub(a, b));
    }

    @ParameterizedTest
    @ValueSource(strings = { "APRIL", "JUNE", "SEPTEMBER", "NOVEMBER" })
    void someMonths_Are30DaysLong(Month month) {
        final boolean isALeapYear = false;
        assertEquals(30, month.length(isALeapYear));
    }

    @ParameterizedTest
    @ValueSource(strings = { "A", "D", "16" })
    public void shouldReturnIntToHex(@ConvertWith(HexToInt.class) int a) {
        assertEquals(-1 * a, calculator.reverseSign(a));
    }

    static class HexToInt extends SimpleArgumentConverter {
        @Override
        protected Object convert(Object o, Class<?> targetType) throws ArgumentConversionException {
            assertEquals(int.class, targetType, "Message");
            return Integer.decode("0x" + o.toString());
        }
    }

}
