package com.example.demoMvcBoot.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.demoMvcBoot.beans.Employee;

@ExtendWith(MockitoExtension.class)
class EmployeeAgencyTest {

    @Mock
    ExternalAgencyService externalAgencyService;

    @InjectMocks
    EmployeeAgency employeeAgency;

    @Test
    public void ShouldReturnListOfEmployeesByTheirAges(){
        //given
        Employee employee1 = new Employee();
        Employee employee2 = new Employee();

        List<Employee> employees = new ArrayList<>();
        employees.add(employee1);
        employees.add(employee2);
        when(externalAgencyService.getEmployees(20,25)).thenReturn(employees);

        //when:
        List<Employee> result = employeeAgency.getEmployeesinRange(20, 25);
        //then:
        assertFalse(result.isEmpty());
        assertEquals(2, result.size());
    }


}
