package com.example.demoMvcBoot.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Month;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class UserServiceSpec {


    Calculator2 calculator = new Calculator2();

    @BeforeEach
    public void setUp() {
        System.out.println("Run before each test");
    }
    @AfterEach
    public void tearDown() {
        System.out.println("Run after each test");
    }


    @Test
    @RepeatedTest(3)
    public void shouldReturnAddRepeat(){
//given
        Calculator2 calc = new Calculator2();
//when
        double result = calc.add(15, 8);
//then
        assertEquals(23, result);
    }



    @Test
    public void assertionExamples(){

        assertEquals(64, 2 * 32);
        int x= 5;
        int y = 20;
        assertTrue(x<y);
        assertFalse(x>y);

        String[] array1 = new String[3];
        array1[0] = "asd";
        String[] array2 = new String[3];
        array2[0] = "asd";
        assertArrayEquals(array1, array2);
       // assertEquals(1, 2);
       /* assertEquals("Values are not equal", 1, 2);
        assertTrue(condition);
        assertFalse(condition);
        assertArrayEquals(array1, array2);
        assertNull(object);
        assertNotNull(object);
        assertSame(object1, object2);
*/
    }


    @Test
    public void shouldReturnMultiplicationOperation(){
        Calculator2 calculator = new Calculator2();
        assertAll(
            () -> assertEquals(4, calculator.multiply(2,2)),
            () -> assertEquals(81, calculator.multiply(9,9)),
            () -> assertEquals(30, calculator.multiply(5,6))
        );
    }


    @Test
    public void shouldAcceptDivideByZero(){
        Calculator2 calculator = new Calculator2();
        IllegalArgumentException exception =
            Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    calculator.divide(10,0);
                });
        assertEquals("Divide by 0", exception.getMessage());
    }


    @ParameterizedTest
    @ValueSource(doubles = {10.0, -23.0, 12.0, -2.0})
    public void shouldReturnReverseSign(double a){
        assertEquals(-1 * a, calculator.reverseSign(a));
    }



    @ParameterizedTest
    @MethodSource(value = "getParameters")
    public void shouldReturnReverseSing2(double a){
        assertEquals(-1 * a, calculator.reverseSign(a));
    }
    static Stream getParameters(){
        return Stream.of(1.0, -231.0, 26.0, -98.0, 100.0);
    }



    @ParameterizedTest
    @ArgumentsSource(MyArgumentsProvider.class)
    void testWithArgumentsSource(String argument) {
        assertNotNull(argument);
    }

    static class MyArgumentsProvider implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
            return Stream.of("apple", "banana").map(Arguments::of);
        }
    }

    @ParameterizedTest
    @ArgumentsSource(Parameters.class)
    public void shouldReturnReverseSing3(double a){
        assertEquals(-1 * a, calculator.reverseSign(a));
    }

    static class Parameters implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext containerExtensionContext) throws Exception{
            return Stream.of(23.0, -56.0, 64.92, -0.32).map(Arguments::of);
        }
    }


    @ParameterizedTest
    @CsvSource({"10.0, 10.0, 20.0", "13.4, 2.9, 16.3", "123.2, 5.3, 128.5"})
    public void shouldReturnAdditionOperation(double a, double b, double sum){
        assertEquals(sum, calculator.add(a, b));
    }


    @ParameterizedTest
    @CsvFileSource(resources = "/dataSource.csv")
    public void shouldReturnSubtractionOperation(double a, double b, double
        sub){
        assertEquals(sub, calculator.sub(a, b));
    }
    @ParameterizedTest
    @ValueSource(strings = {"APRIL", "JUNE", "SEPTEMBER", "NOVEMBER"})
    void someMonths_Are30DaysLong(Month month) {
        final boolean isALeapYear = false;
        assertEquals(30, month.length(isALeapYear));
    }


}
