package com.example.demoMvcBoot.beans;

import static com.example.demoMvcBoot.beans.Employee.MINIMAL_SALARY;
import static com.example.demoMvcBoot.utils.BigdecimalUtils.amount;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.demoMvcBoot.utils.BigdecimalUtils;

class EmployeeTest {

    Employee employee;

    @BeforeEach
    public void setUp() {
        employee = new Employee();
        employee.setEmail("example@example.com");
    }

    BigDecimal baseSalary = new BigDecimal("1000.00");

    @Test
    public void creationTest() {
        Employee employee = new Employee();
        assertNotNull(employee);
    }

    @Test
    public void employeeSalaryTest() {
        Employee employee = new Employee();
        employee.setSalary(baseSalary);
        assertEquals(baseSalary, employee.getSalary());
        IllegalArgumentException exception =
            Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    employee.setSalary(new BigDecimal("-300"));
                });
        assertEquals("Salary could not be negative", exception.getMessage());
        employee.setSalary(amount(200));
        assertEquals(MINIMAL_SALARY, employee.getSalary());
    }

    @Test
    public void employeeAgeTest() {
        Employee employee = new Employee();
        employee.setAge(20);
        assertEquals(20, employee.getAge());
        IllegalArgumentException exception =
            Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    employee.setAge(80);
                });
        assertEquals("Age not in range", exception.getMessage());

        exception =
            Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    employee.setAge(-20);
                });
        assertEquals("Age not in range", exception.getMessage());
    }

    @Test
    public void employeeMailTest() {
        assertEquals("example@example.com", employee.getEmail());

        employee.setEmail("test@inbox.lv");
        assertEquals("test@inbox.lv", employee.getEmail());

        IllegalArgumentException exception =
            Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    employee.setEmail("example.example.com");
                });
        assertEquals("Email is not valid", exception.getMessage());
    }

    @Test
    public void employeeMailTest2() {
        assertEquals("example@example.com", employee.getEmail());

    }

    @Test
    public void employeeMailTest3() {
        IllegalArgumentException exception =
            Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    employee.setEmail("example.example.com");
                });
        assertEquals("Email is not valid", exception.getMessage());
    }

    @Test
    public void employeePhoneTest() {
        Employee employee = new Employee();
        employee.setPhone("+37226663456");
        assertEquals("+37226663456", employee.getPhone());

        IllegalArgumentException exception =
            Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    employee.setPhone("+37526663456");
                });
        assertEquals("Phone is not valid", exception.getMessage());

        exception =
            Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    employee.setPhone("375266634561");
                });
        assertEquals("Phone is not valid", exception.getMessage());
    }

}
